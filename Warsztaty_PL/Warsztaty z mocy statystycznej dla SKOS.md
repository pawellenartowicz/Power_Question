## Opis

Warsztaty poświęcone będą zagadnieniu **mocy statystycznej**. Nauczymy się jak dokonywać symulacji mocy statystycznej. Poznamy teorię wnioskowania statystycznego Jerzego Neymana i Egona Pearsona, która, choć jest powszechnie wykorzystywana w nauce, to jest zdecydowanie zbyt rzadko przytaczana. W końcu poruszymy najczęstsze problemy związane z mocą statystyczną.

Warsztaty będą miały formę konwersatoryjno-ćwiczeniową. Będą przeprowadzanie online, pomocą będzie aplikacji Shiny.app, a dla zainteresowanych kod w R i Python do samodzielnego wywołania.
Nie będzie wymagana umiejętności programowania, ani nawet znajomości programów statystycznych (choć wskazana jest znajomość pojęć zawartych w [Niezbędniku statystycznym](Niezbędnik%20statystyczny.md)


## Dla kogo?

Program jest stworzony z myślą o studentach kierunków społecznych, zwłaszcza tych którzy, będą projektować własny badania (do prac empirycznych, licencjackich, magisterskich).
Jest również pomysł krótszego i bardziej zaawansowanego warsztatu dla praktykujących badaczy.

## Cel warsztatów

Po warsztatach uczestnik powinien:
a) rozumieć pojęcie mocy statystycznej i podstawy statystyki frekwencyjnej;
b) umieć obliczyć minimalną liczby uczestników w badaniu w odniesieniu do mocy statystycznej dla potencjalnie każdego (znanego uczestnikowi) modelu statystycznego metodą Monte Carlo;
c) znać potencjalne problemy związane z używaniem statystyki frekwencyjnej;
d) znać podstawowe metody konstruowania badań, tak, by osiągnąć możliwie, jak największą moc statystyczną.

## Plan:

Godzina | Blok  | Temat |Czas
 :---:|:---:|---|---:
 09:00 | **Test**|*test oprogramowania* |30 min.
 10:00 | **Wprowadzenie** | *Po co są nam testy statystyczne?* |1:30h
  ~ | ~ | *TOST Neymana-Pearsona i Exact Test Fishera*| ~ 
  ~ | ~ |*Czym jest &#945 i &#946, moc testu* | ~ 
  11:30 | Przerwa |Krótka przerwa | 30 min.
 12:00 | **Ćwiczenie 1**| *Modele z jedną zmienną*| 1:30h
  ~ | ~ | *Siła efektu a moc statystyczna*| ~ 
  ~ | ~ | Dyskusja: *badanie konfirmacyjne a eksploracyjne* | ~
  13:30 | Przerwa |długa przerwa | 60 min.
 14:30 | **Ćwiczenie 2**| *Modele ze zmienną binarną*| 1:30h
  ~ | ~ | *Modele ze zmienną ciągłą*| ~ 
  ~ | ~ | Dyskusja: *czy zostawiać zmienne nieistotne?* | ~
  16:00 | Przerwa |Krótka przerwa | 30 min.
 16:30 | **Podsumowanie**| *Złożone modele - symulacja ogólna* | 1:30h
  ~ | ~ | *Jak zmniejszyć potrzebną liczbę obserwacji?*| ~  
  ~ | ~ | Dyskusja: *czy stosować hipotezy kierunkowe, czy nie?*| ~ 
  ~ | ~ | *Potencjalne pułapki: niskie 'prior probality'* | ~
  ~ | ~ | *Potencjalne pułapki: niska moc* | ~
  ~ | ~ | *Potencjalne pułapki: wysoka moc i p-value* | ~
 18:00 | Zakończenie

 
